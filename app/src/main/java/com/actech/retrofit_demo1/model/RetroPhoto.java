package com.actech.retrofit_demo1.model;

import com.google.gson.annotations.SerializedName;

public class RetroPhoto {

    @SerializedName("name")
    private String name;
    @SerializedName("image")
    private String image;

    public RetroPhoto(String name, String image) {
        this.name = name;
        this.image = image;

    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image= image;
    }
}
